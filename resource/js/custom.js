$(document).ready(function(){
	$('.satisfied .video iframe').css('width','100%');
	var wdth = $('.satisfied .video iframe').width();
	$('.satisfied .video iframe').css('height', wdth*0.5625);

	function eqBl(first, second){
		setTimeout(function(){			
			if($(first).height()<$(second).height()){
				$(first).css('height', $(second).height());
			} 
			if($(first).height()>$(second).height()) {
				$(second).css('height', $(first).height());
			}	
		}, 500);
	}

	function eqBlMany(){
		var first = '.offer .row.first .col-md-4:first-child .img_block', 
			second = '.offer .row.first .col-md-4:nth-child(2) .img_block',
			third = '.offer .row.first .col-md-4:nth-child(3) .img_block';
		setTimeout(function(){
			if($(first).height()>$(second).height() && $(first).height()>$(third).height()){
				$(second).css('height', $(first).height());
				$(third).css('height', $(first).height());
			} else if ($(second).height()>$(first).height() && $(second).height()>$(third).height()) {
				$(first).css('height', $(second).height());
				$(third).css('height', $(second).height());
			} else {
				$(first).css('height', $(third).height());
				$(second).css('height', $(third).height());
			}
		}, 500);
	}

	if($(window).width()>767){
		eqBl('.satisfied .row.first .left .img_block','.satisfied .row.first .right .img_block');
		eqBl('.satisfied .row.first .left .p_block','.satisfied .row.first .right .p_block');
		eqBl('.satisfied .row.second .left .img_block','.satisfied .row.second .right .img_block');
		eqBl('.satisfied .row.second .left .p_block','.satisfied .row.second .right .p_block');
		eqBl('.satisfied .row.third .left .img_block','.satisfied .row.third .right .img_block');
		eqBl('.satisfied .row.third .left .p_block','.satisfied .row.third .right .p_block');

		eqBl('.offer .row.second .col-md-4:first-child .img_block','.offer .row.second .col-md-4:last-child .img_block');
		eqBlMany();
	}

	if($(window).width()>991){
		eqBl('.how .row:first-child > .col-md-6:last-child .img_block','.how .row:first-child > .col-md-6:last-child .body');
		eqBl('.how .row:first-child > .col-md-6:first-child .img_block','.how .row:first-child > .col-md-6:first-child .body');

		eqBl('.how .row:nth-child(2) > .col-md-6:last-child .img_block','.how .row:nth-child(2) > .col-md-6:last-child .body');
		eqBl('.how .row:nth-child(2) > .col-md-6:first-child .img_block','.how .row:nth-child(2) > .col-md-6:first-child .body');

		eqBl('.how .row:nth-child(3) > .col-md-6:last-child .img_block','.how .row:nth-child(3) > .col-md-6:last-child .body');
		eqBl('.how .row:nth-child(3) > .col-md-6:first-child .img_block','.how .row:nth-child(3) > .col-md-6:first-child .body');

		eqBl('.how .row:nth-child(4) > .col-md-6:last-child .img_block','.how .row:nth-child(4) > .col-md-6:last-child .body');
		eqBl('.how .row:nth-child(4) > .col-md-6:first-child .img_block','.how .row:nth-child(4) > .col-md-6:first-child .body');

		eqBl('.how .row:last-child > .col-md-6:last-child .img_block','.how .row:last-child > .col-md-6:last-child .body');
		eqBl('.how .row:last-child > .col-md-6:first-child .img_block','.how .row:last-child > .col-md-6:first-child .body');

		eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:last-child');
		eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .body');
		eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .img_block');
		eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .body');
		eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .img_block');

		eqBl('.how .row:nth-child(2) > .col-md-6:first-child','.how .row:nth-child(2) > .col-md-6:last-child');
		eqBl('.how .row:nth-child(2) > .col-md-6:first-child','.how .row:nth-child(2) > .col-md-6:first-child .body');
		eqBl('.how .row:nth-child(2) > .col-md-6:first-child','.how .row:nth-child(2) > .col-md-6:first-child .img_block');
		eqBl('.how .row:nth-child(2) > .col-md-6:last-child','.how .row:nth-child(2) > .col-md-6:last-child .body');
		eqBl('.how .row:nth-child(2) > .col-md-6:last-child','.how .row:nth-child(2) > .col-md-6:last-child .img_block');

		eqBl('.how .row:nth-child(3) > .col-md-6:first-child','.how .row:nth-child(3) > .col-md-6:last-child');
		eqBl('.how .row:nth-child(3) > .col-md-6:first-child','.how .row:nth-child(3) > .col-md-6:first-child .body');
		eqBl('.how .row:nth-child(3) > .col-md-6:first-child','.how .row:nth-child(3) > .col-md-6:first-child .img_block');
		eqBl('.how .row:nth-child(3) > .col-md-6:last-child','.how .row:nth-child(3) > .col-md-6:last-child .body');
		eqBl('.how .row:nth-child(3) > .col-md-6:last-child','.how .row:nth-child(3) > .col-md-6:last-child .img_block');

		eqBl('.how .row:nth-child(4) > .col-md-6:first-child','.how .row:nth-child(4) > .col-md-6:last-child');
		eqBl('.how .row:nth-child(4) > .col-md-6:first-child','.how .row:nth-child(4) > .col-md-6:first-child .body');
		eqBl('.how .row:nth-child(4) > .col-md-6:first-child','.how .row:nth-child(4) > .col-md-6:first-child .img_block');
		eqBl('.how .row:nth-child(4) > .col-md-6:last-child','.how .row:nth-child(4) > .col-md-6:last-child .body');
		eqBl('.how .row:nth-child(4) > .col-md-6:last-child','.how .row:nth-child(4) > .col-md-6:last-child .img_block');

		eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:last-child');
		eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .body');
		eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .img_block');
		eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .body');
		eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .img_block');
	}

	$('.satisfied .video iframe').css('width','100%');
	var wdth = $('.satisfied .video iframe').width();
	$('.satisfied .video iframe').css('height', wdth*0.5625);

	$(window).resize(function(){
		$('.satisfied .video iframe').css('width','100%');
		var wdth = $('.satisfied .video iframe').width();
		$('.satisfied .video iframe').css('height', wdth*0.5625);

		if($(window).width()>767){
			eqBl('.satisfied .row.first .left .img_block','.satisfied .row.first .right .img_block');
			eqBl('.satisfied .row.first .left .p_block','.satisfied .row.first .right .p_block');
			eqBl('.satisfied .row.second .left .img_block','.satisfied .row.second .right .img_block');
			eqBl('.satisfied .row.second .left .p_block','.satisfied .row.second .right .p_block');
			eqBl('.satisfied .row.third .left .img_block','.satisfied .row.third .right .img_block');
			eqBl('.satisfied .row.third .left .p_block','.satisfied .row.third .right .p_block');

			eqBl('.offer .row.second .col-md-4:first-child .img_block','.offer .row.second .col-md-4:last-child .img_block');
			eqBlMany(); 

			$('.satisfied .video iframe').css('width','100%');
			var wdth = $('.satisfied .video iframe').width();
			$('.satisfied .video iframe').css('height', wdth*0.5625);
		} else {
			$('.satisfied .row .img_block').css('height','initial');
			$('.satisfied .row .p_block').css('height','initial');
		}

		if($(window).width()>991){
			eqBl('.how .row:first-child > .col-md-6:last-child .img_block','.how .row:first-child > .col-md-6:last-child .body');
			eqBl('.how .row:first-child > .col-md-6:first-child .img_block','.how .row:first-child > .col-md-6:first-child .body');

			eqBl('.how .row:nth-child(2) > .col-md-6:last-child .img_block','.how .row:nth-child(2) > .col-md-6:last-child .body');
			eqBl('.how .row:nth-child(2) > .col-md-6:first-child .img_block','.how .row:nth-child(2) > .col-md-6:first-child .body');

			eqBl('.how .row:nth-child(3) > .col-md-6:last-child .img_block','.how .row:nth-child(3) > .col-md-6:last-child .body');
			eqBl('.how .row:nth-child(3) > .col-md-6:first-child .img_block','.how .row:nth-child(3) > .col-md-6:first-child .body');

			eqBl('.how .row:nth-child(4) > .col-md-6:last-child .img_block','.how .row:nth-child(4) > .col-md-6:last-child .body');
			eqBl('.how .row:nth-child(4) > .col-md-6:first-child .img_block','.how .row:nth-child(4) > .col-md-6:first-child .body');

			eqBl('.how .row:last-child > .col-md-6:last-child .img_block','.how .row:last-child > .col-md-6:last-child .body');
			eqBl('.how .row:last-child > .col-md-6:first-child .img_block','.how .row:last-child > .col-md-6:first-child .body');

			eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:last-child');
			eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .body');
			eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .img_block');
			eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .body');
			eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .img_block');

			eqBl('.how .row:nth-child(2) > .col-md-6:first-child','.how .row:nth-child(2) > .col-md-6:last-child');
			eqBl('.how .row:nth-child(2) > .col-md-6:first-child','.how .row:nth-child(2) > .col-md-6:first-child .body');
			eqBl('.how .row:nth-child(2) > .col-md-6:first-child','.how .row:nth-child(2) > .col-md-6:first-child .img_block');
			eqBl('.how .row:nth-child(2) > .col-md-6:last-child','.how .row:nth-child(2) > .col-md-6:last-child .body');
			eqBl('.how .row:nth-child(2) > .col-md-6:last-child','.how .row:nth-child(2) > .col-md-6:last-child .img_block');

			eqBl('.how .row:nth-child(3) > .col-md-6:first-child','.how .row:nth-child(3) > .col-md-6:last-child');
			eqBl('.how .row:nth-child(3) > .col-md-6:first-child','.how .row:nth-child(3) > .col-md-6:first-child .body');
			eqBl('.how .row:nth-child(3) > .col-md-6:first-child','.how .row:nth-child(3) > .col-md-6:first-child .img_block');
			eqBl('.how .row:nth-child(3) > .col-md-6:last-child','.how .row:nth-child(3) > .col-md-6:last-child .body');
			eqBl('.how .row:nth-child(3) > .col-md-6:last-child','.how .row:nth-child(3) > .col-md-6:last-child .img_block');

			eqBl('.how .row:nth-child(4) > .col-md-6:first-child','.how .row:nth-child(4) > .col-md-6:last-child');
			eqBl('.how .row:nth-child(4) > .col-md-6:first-child','.how .row:nth-child(4) > .col-md-6:first-child .body');
			eqBl('.how .row:nth-child(4) > .col-md-6:first-child','.how .row:nth-child(4) > .col-md-6:first-child .img_block');
			eqBl('.how .row:nth-child(4) > .col-md-6:last-child','.how .row:nth-child(4) > .col-md-6:last-child .body');
			eqBl('.how .row:nth-child(4) > .col-md-6:last-child','.how .row:nth-child(4) > .col-md-6:last-child .img_block');

			eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:last-child');
			eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .body');
			eqBl('.how .row:last-child > .col-md-6:first-child','.how .row:last-child > .col-md-6:first-child .img_block');
			eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .body');
			eqBl('.how .row:last-child > .col-md-6:last-child','.how .row:last-child > .col-md-6:last-child .img_block');
		} else if($(window).width()<992) {
			$('.how .col-md-6').css('height','initial');
			$('.how .img_block').css('height','initial');
			$('.how .body').css('height','initial');
		}
	});
});