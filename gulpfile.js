'use strict';

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	maps = require('gulp-sourcemaps'),
	del = require('del'),
	cssconcat = require('gulp-concat-css'),
	cssmin = require('gulp-clean-css'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	notify = require('gulp-notify'),
	htmlmin = require('gulp-htmlmin'),
    fontmin = require('gulp-fontmin'),
    gzip = require('gulp-gzip');

gulp.task("concatCss", function(){
	return gulp.src(["resource/css/bootstrap.min.css",
		"resource/css/jquery.fancybox.css",
		"resource/css/parsley.css",		
		"resource/css/style.min.css",
		"resource/css/project_styles.css"])
	.pipe(cssconcat("styles.css"))
	.pipe(gulp.dest("css"));
});

gulp.task('minifyCss', ['concatCss'], function(){
	return gulp.src('css/styles.css')
	.pipe(maps.init())
	.pipe(cssmin())
	.pipe(rename('styles.min.css'))
	.pipe(maps.write(''))
	.pipe(notify('Уменьшение CSS прошло успешно!'))
	.pipe(gulp.dest('css'));
});

gulp.task("concatScripts", function(){
	return gulp.src(["resource/js/jquery-2.1.4.min.js",
		"resource/js/bootstrap.min.js",
		"resource/js/jquery.fancybox.pack.js",
		"resource/js/parsley.js",
		"resource/js/form.js",
		"resource/js/custom.js"])
	.pipe(maps.init())
	.pipe(concat("app.js"))
	.pipe(maps.write(''))
	.pipe(gulp.dest("js"));
});

gulp.task('minifyScripts', ['concatScripts'], function(){
	return gulp.src('js/app.js')
	.pipe(uglify())
	.pipe(rename('app.min.js'))
	.pipe(notify('Уменьшение JS прошло успешно!'))
	.pipe(gulp.dest('js'));
});

gulp.task('imagesMin', function(){
	return gulp.src(['resource/images/**/*'])
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(notify('Уменьшение IMAGES прошло успешно!'))
		.pipe(gulp.dest('images'));
});

gulp.task('minifyFonts', function () {
    return gulp.src('resource/fonts/*.ttf')
        .pipe(fontmin({
            text: '天地玄黄 宇宙洪荒',
        }))
		.pipe(notify('Уменьшение Fonts прошло успешно!'))
        .pipe(gulp.dest('fonts'));
});

gulp.task('minifyHtml', function() {
  return gulp.src('resource/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
	.pipe(notify('Уменьшение HTML прошло успешно!'))
    .pipe(gulp.dest(''))
});

gulp.task('copy-php', function() {
   return gulp.src('resource/*.php')
   .pipe(gulp.dest(''));
});

gulp.task('copy-htaccess', function() {
   return gulp.src('resource/.htaccess')
   .pipe(gulp.dest(''));
});

gulp.task('watch', function(){
	gulp.watch(['resource/css/*.css'], ['minifyCss']);
	gulp.watch(['resource/js/*.js'], ['minifyScripts']);
	gulp.watch(['resource/images/**/*'], ['imagesMin']);
	gulp.watch(['resource/*.html'], ['minifyHtml']);
	gulp.watch(['resource/fonts/*.ttf'], ['minifyFonts']);
	gulp.watch(['resource/*.php'], ['copy-php']);
	gulp.watch(['resource/.htaccess'], ['copy-htaccess']);
});

gulp.task('clean', function(){
	del(['dist']);
})

gulp.task("build",['minifyScripts','minifyCss','imagesMin','minifyHtml','minifyFonts','copy-php','copy-htaccess'], function(){
	return gulp.src(["css/styles.min.css", "js/app.min.js","images/**/*","fonts/*.ttf",'*.html','*.php','.htaccess'],
				 {base: './'})
			.pipe(gulp.dest(''));
});

gulp.task('serve',['watch']);

gulp.task("default", ["clean"], function(){
	gulp.start('build');
});